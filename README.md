# Blog Website - Laravel Blog App

This is the source code of a simple laravel blog website. It is a website with a blog application. It also includes full authentication and file uploading.

## Version
1.0.0

## Database
The sql dump is in _SQL/lsapp.sql